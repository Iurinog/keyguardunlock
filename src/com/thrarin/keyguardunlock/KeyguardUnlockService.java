
package com.thrarin.keyguardunlock;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

@SuppressWarnings("deprecation")
public class KeyguardUnlockService extends Service {

    private KeyguardManager.KeyguardLock mKeyguard;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(KeyguardUnlockService.class.getName(), "OnCreate method");

        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        mKeyguard = km.newKeyguardLock("KeyguardUnlockService");
        mKeyguard.disableKeyguard();
    }

    @Override
    public void onDestroy() {

        Log.d(KeyguardUnlockService.class.getName(), "OnDestroy method");
        mKeyguard.reenableKeyguard();
        super.onDestroy();
    }
}
