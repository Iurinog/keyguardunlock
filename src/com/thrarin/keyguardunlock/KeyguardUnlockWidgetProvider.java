
package com.thrarin.keyguardunlock;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class KeyguardUnlockWidgetProvider extends AppWidgetProvider {

    private static final String IMAGE_CLICK = "KEYGUARD_UNLOCK_IMAGE_CLICK";

    private static boolean isUnlocked = false;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        // Set Image click pending intent
        remoteView.setOnClickPendingIntent(R.id.widget_image,
                getPendingIntent(context, appWidgetIds[0]));

        appWidgetManager.updateAppWidget(appWidgetIds[0], remoteView);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        String action = intent.getAction();
        Log.d(KeyguardUnlockWidgetProvider.class.getName(), "OnReceive: Action: " + action);
        Log.d(KeyguardUnlockWidgetProvider.class.getName(), "isUnlocked: : " + isUnlocked);

        if (action.equalsIgnoreCase(IMAGE_CLICK)) {

            RemoteViews remoteView = new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout);

            if (isUnlocked) {
                isUnlocked = false;
                remoteView.setImageViewResource(R.id.widget_image, R.drawable.off);
                stopUnlockKeyguardService(context);
                Toast.makeText(context, "Keyguard enable", Toast.LENGTH_SHORT).show();
            } else {
                isUnlocked = true;
                remoteView.setImageViewResource(R.id.widget_image, R.drawable.on);
                startUnlockKeyguardService(context);
                Toast.makeText(context, "Keyguard disable", Toast.LENGTH_SHORT).show();
            }

            appWidgetManager.updateAppWidget(
                    intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, 0), remoteView);
        }

    }

    private PendingIntent getPendingIntent(Context context, int appWidgetId) {

        Intent intent = new Intent(context, KeyguardUnlockWidgetProvider.class);

        intent.setAction(KeyguardUnlockWidgetProvider.IMAGE_CLICK);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetId);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;

    }

    private void startUnlockKeyguardService(Context context) {

        Intent sIntent = new Intent(context.getApplicationContext(), KeyguardUnlockService.class);
        context.startService(sIntent);
    }

    private void stopUnlockKeyguardService(Context context) {

        Intent sIntent = new Intent(context.getApplicationContext(), KeyguardUnlockService.class);
        context.stopService(sIntent);

    }
}
