LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCASE_NAME := $(shell echo $(TARGET_DEVICE) | tr A-Z a-z)
LOCASE_TARGET_MODEL := $(shell echo $(TARGET_MODEL) | tr A-Z a-z)
LOCASE_TARGET_PRODUCT := $(shell echo $(TARGET_PRODUCT) | tr A-Z a-z)
LOCAL_PACKAGE_NAME := KeyguardUnlock

include $(BUILD_PACKAGE)
include $(call all-makefiles-under,$(LOCAL_PATH))
